package model

type WxPayInfo struct {
	Id              int64  `gorm:"primary_key;not null;auto_increment"` // 主键
	PlatformOrderId string `gorm:"index:idx_pay_platform_order_id"`     // 寻光支付平台唯一订单ID
	ThreeOrderId    string `gorm:"index:idx_pay_three_order_id`         // 三方平台支付ID，加普通索引
	PayType			int 												// 三方充值类型。 0: 小程序  1: H5
	OpenId          string `gorm:"index:idx_pay_openid"`                // 加唯一引所,寻光钱包的open ID
	H5Openid		string												// H5 的 openId
	UserId          string `gorm:"index:idx_pay_userid"`                // 游戏玩家ID
	Paymoney        string                                              // 充值金额
	MchId           string                                              // 商户信息
	Appid           string                                              // 集五福app ID
	Package         string                                              //
	NonceStr        string                                              //
	TimeStamp       string                                              // 发送客户端的时间戳
	SignType        string                                              //
	PaySign         string                                              //
	PrepayID        string                                              // 微信统一下单ID
	Synced          int    `gorm:"index:idx_pay_synced"`                // 0:未同步,1已同步
	Finishedtime    string                                              // 完成时间
	Sort            string                                              // 游戏充值类型
	Value           string                                              // 游戏充值参数
	Exvalue         string `gorm:"index:idx_pay_exvalue"`               // 额外参数（当作游戏ID）
	OutTradeNo      string                                              // 微信回调订单ID
	NoticeState     bool                                                // 是否已通知前端，弱网校验机制
	GameId          string `gorm:"index:idx_pay_gameId"`                // 游戏ID
	DeviceType      string												// 设备信息  0: Android   1: iOS
	PlId            uint64                                              // 支付小程序的主键 ID
	ChannelKey      string                                              // 接入的渠道 ID
	BasicChannelId  string												// 用户原始的进入渠道
	AdKeyId         string												// 广告位原始进入的id
	Uuid            string												// 打点的 uuid
	AdKey           string												// 广告位 key
	OrderState      int                                                 // 订单状态 1: 订单生效 0: 订单失效(已被使用过)
	BackUrl         string                                              // 小游戏服务器回调地址
	NoticeTimes     int                                                 //回调小游戏服务器次数
	CreatedAt       string                                              // 记录创建时间
	UpdatedAt       string                                              // 订单修改时间
}