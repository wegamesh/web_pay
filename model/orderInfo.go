package model

type ScanGameInfo struct {
	ID              uint64
	GameAppid       string
	GameAppsecret   string
	PlId            uint64
	PaybackUrl      string
	GameName        string
	PicUrl          string
	ExValue         string
}

type ScanWxOrder struct {
	ID             uint64
	Synced         int
}

type ScanGameId struct {
	ID            uint64
}