package model

type Msg struct {
	Code    int
	Msg     string
	Data    interface{}
	Cmd     string
}