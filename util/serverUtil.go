package util

const SandBox int = 1  //都可以实现以下功能
const IsLogin int = 2  //登陆服务器 登陆
const IsMain int = 3   //主服务器，对战
const IsAux int = 4    //辅助服务器，刷新排行榜
const IsRemote int = 5 //远程服务器，刷怪和生成战报,生成每个游戏的token

var serverPort string
var serverType int

func SetServerPort(tmp string) {
	serverPort = tmp
}

func SetServerType(tmp int) {
	serverType = tmp
}

func GetServerType() int {
	return serverType
}

func GetServerAddress() string{
	return ":"+serverPort
}
