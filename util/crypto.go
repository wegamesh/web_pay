package util

// 加密解密相关

import (
	"crypto/md5"
	"encoding/hex"
)


/**
 * MD5加密，两次加盐加密
 * @param: str string 待加密的字符串
 */
func MD5Crypto(str string) string {

	md5Ctx := md5.New()
	md5Ctx.Write([]byte(str))
	cipherStr := md5Ctx.Sum(nil)
	md5Str := hex.EncodeToString(cipherStr)

	return md5Str
}

