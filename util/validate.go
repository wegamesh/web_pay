package util

/**
 * 验证相关
 */

import (
	"regexp"
)

const (
	//手机号正则
	regularPhone = `^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(18[0,5-9]))\d{8}$`
	//密码正则
	regularPassword = `^[\w\x21-\x7e]{6,16}$`
)

/**
 * 验证手机号
 * @param: phone string 手机号
 */
func ValidatePhone(phone string) bool {
	var regularExp string
	regularExp = regularPhone
	reg := regexp.MustCompile(regularExp)
	return reg.MatchString(phone)
}

/**
 * 验证密码
 * @param: password string 密码
 */
func ValidatePassword(password string) bool {
	var regularExp string
	regularExp = regularPassword
	reg := regexp.MustCompile(regularExp)
	return reg.MatchString(password)
}
