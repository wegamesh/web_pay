package util

import (
	"log"
)

//sort对应打印输出日志的等级
//sort等于0，调试的时候打印的日志。
//sort等于1，正常的打印日志
//sort等于2，警告的打印日志
//sort等于3，错误的打印日志
//userId就是用户的UserId
//v ...是输出位置标志，和错误原因等等信息
func PrintLogger(sort int, appId string, v ...interface{}) {
	preFix := ""
	switch sort {
	case 0:
		preFix = "[DEBUG]+APPID:" + appId
		log.Printf(preFix, v...)
	case 1:
		preFix = "[INFO]+APPID:" + appId
		log.Printf(preFix, v...)
	case 2:
		preFix = "[WARN]+APPID:" + appId
		log.Printf(preFix, v...)
	case 3:
		preFix = "[ERROR]+APPID:" + appId
		log.Printf(preFix, v...)
	default:
		preFix = "[DEFAULT]+APPID:" + appId
		log.Printf(preFix, v...)
	}
}
