package util

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"web_pay/config"
)

var WDb *gorm.DB

func WDbinit(env string) *gorm.DB {
	dbUrl := ""

	if env == "production" {
		dbUrl = config.W_DB_PRODUCTIOIN_USER_NAME + ":" + config.W_DB_PRODUCTIOIN_PSW + "@tcp(" + config.W_DB_PRODUCTION_HOST_IP + ")/" + config.W_DB_PRODUCTIOIN_NAME + "?charset=utf8&parseTime=True&loc=Local"

	} else if env == "qa" {
		dbUrl = config.W_DB_QA_USER_NAME + ":" + config.W_DB_QA_PSW + "@tcp(" + config.W_DB_QA_HOST_IP + ")/" + config.W_DB_QA_NAME + "?charset=utf8&parseTime=True&loc=Local"

	} else if env == "develop" {
		dbUrl = config.W_DB_DEV_USER_NAME + ":" + config.W_DB_DEV_PSW + "@tcp(" + config.W_DB_DEV_HOST_IP + ")/" + config.W_DB_DEV_NAME + "?charset=utf8&parseTime=True&loc=Local"

	} else if env == "self" {
		dbUrl = config.W_DB_DEV_USER_NAME + ":" + config.W_DB_DEV_PSW + "@tcp(" + config.W_DB_DEV_HOST_IP + ")/" + "travel_self" + "?charset=utf8&parseTime=True&loc=Local"

	} else if env == "kai" {
		dbUrl = config.W_DB_KAI_USER_NAME + ":" + config.W_DB_KAI_PSW + "@tcp(" + config.W_DB_KAI_HOST_IP + ")/" + config.W_DB_KAI_NAME + "?charset=utf8&parseTime=True&loc=Local"

	} else {
		panic("没有编译参数,或者不对")
	}

	//fmt.Println("dbUrl", dbUrl)
	db, err := gorm.Open("mysql", dbUrl)
	if err != nil {
		panic(err)
		fmt.Println("数据库连接失败！！！")
		//panic("数据库连接失败！！！")
	}
	WDb = db
	return WDb
}
