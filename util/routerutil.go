package util

import (
	"web_pay/config"

	"github.com/labstack/echo"
)

func InitRouter(env string, app *echo.Echo) *echo.Group {
	if env == "production" {
		return app.Group(config.ROUTER_PRD)

	} else if env == "qa" {
		return app.Group(config.ROUTER_QA)

	} else if env == "develop" {
		return app.Group(config.ROUTER_DEV)

	} else if env == "kai" {
		return app.Group(config.ROUTER_DEV)

	} else if env == "edu" {
		return app.Group(config.ROUTER_DEV)

	} else {
		app.Logger.Error("app  InitRouter 错误")
	}
	return nil

}
