package util

import (
	"web_pay/config"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"

	"fmt"
)

var Db *gorm.DB

func Dbinit(env string) *gorm.DB {
	dbUrl := ""

	if env == "production" {
		dbUrl = config.DB_PRODUCTIOIN_USER_NAME + ":" + config.DB_PRODUCTIOIN_PSW + "@tcp(" + config.DB_PRODUCTION_HOST_IP + ")/" + config.DB_PRODUCTIOIN_NAME + "?charset=utf8&parseTime=True&loc=Local"

	} else if env == "qa" {
		dbUrl = config.DB_QA_USER_NAME + ":" + config.DB_QA_PSW + "@tcp(" + config.DB_QA_HOST_IP + ")/" + config.DB_QA_NAME + "?charset=utf8&parseTime=True&loc=Local"

	} else if env == "develop" {
		dbUrl = config.DB_DEV_USER_NAME + ":" + config.DB_DEV_PSW + "@tcp(" + config.DB_DEV_HOST_IP + ")/" + config.DB_DEV_NAME + "?charset=utf8&parseTime=True&loc=Local"

	} else if env == "kai" {
		dbUrl = config.DB_KAI_USER_NAME + ":" + config.DB_KAI_PSW + "@tcp(" + config.DB_KAI_HOST_IP + ")/" + config.DB_KAI_NAME + "?charset=utf8mb4&parseTime=True&loc=Local"

	} else if env == "edu" {
		dbUrl = config.DB_EDU_USER_NAME + ":" + config.DB_EDU_PSW + "@tcp(" + config.DB_EDU_HOST_IP + ")/" + config.DB_EDU_NAME + "?charset=utf8mb4&parseTime=True&loc=Local"

	} else {
		panic("没有编译参数,或者不对")
	}

	db, err := gorm.Open("mysql", dbUrl)
	if err != nil {
		fmt.Println(err)
		fmt.Println("数据库连接失败！！！")
	}
	Db = db
	fmt.Println(Db)
	return Db
}
