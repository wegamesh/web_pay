package util

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

/**
 * 根据 , 切割字符串
 */
func SplitStrByComma(originStr string) []string {
	return strings.Split(originStr, ",")
}

/**
 * 获取 Int 类型的时间戳
 */
func GetIntTypeTimeStamp() int64 {
	return time.Now().Unix()
}

/**
 * 获取 Unix 的时间戳
 */
func GetUnixTimeStamp() string {
	currTime := time.Now().Unix()
	return strconv.FormatInt(currTime, 10)
}

/**
 * 获取到远程url的内容
 **/
func Curl(url string, data interface{}) (string, error) {

	if data != "" {

		postData, _ := JsonEncode(data)

		resp, errNet := http.Post(url, "application/x-www-form-urlencoded", strings.NewReader(postData))

		if errNet != nil {
			return "", errNet
		}

		defer resp.Body.Close()

		body, bodyErr := ioutil.ReadAll(resp.Body)

		strBody := string(body)

		return strBody, bodyErr

	} else {

		resp, errNet := http.Get(url)

		if errNet != nil {
			return "", errNet
		}

		defer resp.Body.Close()

		body, bodyErr := ioutil.ReadAll(resp.Body)

		strBody := string(body)

		return strBody, bodyErr
	}

}

/**
 * JsonEncode 用法：JsonEncode(data)
 */
func JsonEncode(data interface{}) (string, error) {

	// 转json时 防止字符被转义
	jsonByte, err := json.Marshal(data)

	jsonByte = bytes.Replace(jsonByte, []byte("\\u0026"), []byte("&"), -1)
	jsonByte = bytes.Replace(jsonByte, []byte("\\u003c"), []byte("<"), -1)
	jsonByte = bytes.Replace(jsonByte, []byte("\\u003e"), []byte(">"), -1)
	jsonByte = bytes.Replace(jsonByte, []byte("\\u003d"), []byte("="), -1)

	jsonStr := string(jsonByte)

	return jsonStr, err
}

/**
 * JsonDecode 用法：JsonDecode(str, &data)
 */
func JsonDecode(jsonStr string, data interface{}) error {

	jsonErr := json.Unmarshal([]byte(jsonStr), data)

	return jsonErr
}

/**
 * 格式化当前日期，转化为时间戳
 */
func FormatTime2Timestamp(currentDate string) string {
	loc, _ := time.LoadLocation("Local")
	parsedTime, _ := time.ParseInLocation("2006-01-02", currentDate, loc)
	timeStamp := parsedTime.Unix()
	return strconv.FormatInt(timeStamp, 10)
}


/**
 * 时间戳转换成日期，再转换成凌晨0点的时间戳
 */
func TimeConvert2stamp(timeStr string) int64 {
	// 2018-6-3 13:02:21
	timeTimeStamp, _ := strconv.ParseInt(timeStr, 10, 64)
	// 2018-6-3
	formatTimeStr := time.Unix(timeTimeStamp, 0).Format("2006-01-02")

	// 转化成时间戳
	loc, _ := time.LoadLocation("Local")
	formatedTime, _ := time.ParseInLocation("2006-01-02", formatTimeStr, loc)
	corrTime := formatedTime.Unix()

	return corrTime
}

/**
 * 搜索切片中是否存在指定的值。
 */
func InSliceInt(needle int, haystack []int) bool {

	for _, val := range haystack {

		if needle == val { return true }

	}

	return false
}

/**
 * 搜索切片中是否存在指定的值。
 */
func InSliceString(needle string, haystack []string) bool {

	for _, val := range haystack {

		if needle == val { return true }

	}

	return false
}









