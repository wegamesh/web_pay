package util

import (
	"github.com/aiscrm/redisgo"
	"github.com/garyburd/redigo/redis"
	"web_pay/config"
	"web_pay/constant"
)

var RedisClient *redisgo.Redis

func RedisInit(env string) {
	if env == "production" {
		redisgo.New(config.REDIS_PRODUCTION_HOST, config.REDIS_PRODUCTION_PSW, 8)
	} else if env == "qa" {
		redisgo.New(config.REDIS_QA_HOST, config.REDIS_QA_PSW, 9)
	} else if env == "develop" {
		redisgo.New(config.REDIS_DEV_HOST, config.REDIS_DEV_PSW, 3)
	} else if env == "self" {
		redisgo.New(config.REDIS_DEV_HOST, config.REDIS_DEV_PSW, 1)
	} else if env == "edu" {
		redisgo.New(config.REDIS_EDUCAT_HOST, config.REDIS_EDUCAT_PSW, 1)
	} else {
		redisgo.New(config.REDIS_DEV_HOST, config.REDIS_DEV_HOST, 1)
	}

	RedisClient = redisgo.GetInstance()
}

func DoRedis(commandName string, args ...interface{}) (reply interface{}, err error) {
	return RedisClient.Do(commandName, args...)
}

/**
 * SetRedis 用法：SetRedis("key", val)
 */
func SetRedis(key string, val interface{}) (string, error) {

	res, err := redis.String(RedisClient.Set(key, val, constant.REDIS_TIME))

	return res, err
}

/**
 * SetExRedis 用法：SetExRedis("key", val, expire)
 */
func SetExRedis(key string, val interface{}, expire int) (string, error) {

	res, err := redis.String(RedisClient.Set(key, val, expire))

	return res, err
}

/**
 * DelRedis 删除键
 */
func DelRedis(key string) error {

	err := RedisClient.Del(key)

	return err
}

/**
 * ExistsRedis 检查键是否存在
 */
func ExistsRedis(key string) (bool, error) {

	res, err := RedisClient.Exists(key)

	return res, err
}

/**
 * GetRedisString 用法：GetRedisString("key")
 */
func GetRedisString(key string) (string, error) {

	res, err := RedisClient.GetString(key)

	return res, err
}

/**
 * GetRedisInt 用法：GetRedisInt("key")
 */
func GetRedisInt(key string) (int, error) {

	res, err := RedisClient.GetInt(key)

	return res, err
}

/**
 * GetRedisUint64 用法：GetRedisUint64("key")
 */
func GetRedisUint64(key string) (uint64, error) {

	res, err := RedisClient.GetInt64(key)

	return uint64(res), err
}

/**
 * GetRedisBool 用法：GetRedisBool("key")
 */
func GetRedisBool(key string) (bool, error) {

	res, err := RedisClient.GetBool(key)

	return res, err
}

/**
 * GetRedisObject 用法：GetRedisObject("key", val)
 */
func GetRedisObject(key string, val interface{}) error {

	err := RedisClient.GetObject(key, val)

	return err
}

/**
 * GetRedisKeys 用法：GetRedisKeys("key")
 */
func GetRedisKeys(key string) ([]string, error) {

	keys, err := redis.Strings( RedisClient.Do("KEYS", key) )

	return keys, err
}