package wxutil

import (
	"math/rand"
	"time"
	"github.com/labstack/echo"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"encoding/xml"
	"web_pay/model"
	"web_pay/util"
	"web_pay/config"
	"sort"
	"strings"
	"crypto/md5"
	"bytes"
	"io"
	"web_pay/dao"
	"errors"
	"web_pay/common"
)

//产生随机字符串

func GetRandomString(num int) string {
	str := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	bytes := []byte(str)
	result := []byte{}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < num; i++ {
		result = append(result, bytes[r.Intn(len(bytes))])
	}
	return string(result)
}

func JSNotifyCallBack(c echo.Context) error{

	var reXML model.WeChatPayResult
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		reXML.ResultCode = "FAIL"
		reXML.ReturnMsg = "Bodyerror"
		msg, _ := json.Marshal(reXML)
		c.Response().Write(msg)
		util.PrintLogger(3, "", "微信解析出错FAILBodyerror", err)
		return nil
	}
	err = xml.Unmarshal(body, &reXML)
	if err != nil {
		reXML.ReturnMsg = "参数错误"
		reXML.ResultCode = "FAIL"
		msg, _ := json.Marshal(reXML)
		c.Response().Write(msg)
		util.PrintLogger(3, "", "微信解析出错Unmarshal", err)
		return nil
	}
	if reXML.ReturnCode != "SUCCESS" {
		reXML.ResultCode = "FAIL"
		msg, _ := json.Marshal(reXML)
		c.Response().Write(msg)
		return nil
	}

	m := XmlToMap(body)

	util.PrintLogger(3, "", "微信给我回调的东西")
	util.PrintLogger(3, "", "具体是什么", m)

	//处理微信回调成功，游戏相应给用户道具
	result_code, ok1 := m["result_code"]
	return_code, ok2 := m["return_code"]
	if ok1 && result_code == "SUCCESS" && ok2 && return_code == "SUCCESS" {
		openid, ok := m["openid"]
		if ok && openid != "" {
			go HandleWxPaySuccessAddItem(m)
		}
	}

	var signData []string
	for k, v := range m {
		if k == "sign" {
			continue
		}
		signData = append(signData, fmt.Sprintf("%v=%v", k, v))
	}

	key := config.MCHSECRET

	mySign, err := WechatGenSign(key, m)
	if err != nil {
		reXML.ResultCode = "FAIL"
		msg, _ := json.Marshal(reXML)
		c.Response().Write(msg)
		util.PrintLogger(3, "", "WechatGenSign:", err)
		return nil
	}

	if mySign != m["sign"] {
		util.PrintLogger(3, "", "签名交易错误")
	}

	util.PrintLogger(3, "", "交易成功")

	reXML.ResultCode = "SUCCESS"
	msg, _ := json.Marshal(reXML)
	c.Response().Write(msg)

	return nil
}



func XmlToMap(xmlData []byte) map[string]string {
	decoder := xml.NewDecoder(bytes.NewReader(xmlData))
	m := make(map[string]string)
	var token xml.Token
	var err error
	var k string
	for token, err = decoder.Token(); err == nil; token, err = decoder.Token() {
		if v, ok := token.(xml.StartElement); ok {
			k = v.Name.Local
			continue
		}
		if v, ok := token.(xml.CharData); ok {
			data := string(v.Copy())
			if strings.TrimSpace(data) == "" {
				continue
			}
			m[k] = data
		}
	}

	if err != nil && err != io.EOF {
		panic(err)
	}
	return m
}

func HandleWxPaySuccessAddItem(tmp map[string]string) {
	tradeNo := tmp["out_trade_no"]
	if tradeNo == "" {
		util.PrintLogger(3, "", "微信回调订单号为空")
		return
	}
	util.PrintLogger(1, "", "微信回调订单号:", tradeNo)
	openid  := tmp["openid"]
	money   := tmp["total_fee"]
	payList, payListErr := dao.GetWxPayOrderByOpenid(openid, money)

	if nil != payListErr && "record not found" != payListErr.Error() {
		util.PrintLogger(3, "查询订单信息失败", payListErr)
		return
	}

	payLen  := len(payList)
	// 不存在
	if payLen <= 0 {
		util.PrintLogger(3, "", "不存在该玩家订单")
		return
	}

	util.PrintLogger(1, "订单信息到底是啥？", tmp)
	util.PrintLogger(1, "tradeNo？", tradeNo)
	util.PrintLogger(1, "想知道 payList==？", payList)

	// 已处理  TODO: 测试
	for _, pay := range payList {
		if pay.OutTradeNo == tradeNo {
			util.PrintLogger(3, "tradeNo？", tradeNo)

			util.PrintLogger(3, "pay.OutTradeNo？", pay.OutTradeNo)
			util.PrintLogger(3, "", "订单都已经处理")

			return
		}
	}
	// 寻找未处理订单-倒序
	pay := model.WxPayInfo{}
	for i := payLen - 1; i >= 0; i-- {
		elem := payList[i]
		if elem.Synced == 0 && elem.Paymoney == money {
			pay = elem
			break
		}
	}

	util.PrintLogger(1, "pay=========", pay)
	util.PrintLogger(1, "pay.OpenId=========", pay.OpenId)
	// 不存在未处理，即已处理
	if pay.OpenId == "" {
		util.PrintLogger(3, "", "不存在该玩家未处理的订单")
		return
	}
	// 保存订单数据
	pay.OutTradeNo = tradeNo
	pay.Synced = 1
	pay.Finishedtime = fmt.Sprintf("%d", time.Now().Unix())

	util.PrintLogger(1, "需要翻转的订单信息的 openId=========", pay.OpenId)
	util.PrintLogger(1, "需要翻转的订单信息=========", pay)

	err := dao.SaveWxPayOrder(pay)
	if err != nil {
		util.PrintLogger(3, pay.UserId, "保存用户信息到数据库更新修改", err)
		return
	}

	// fix 去掉三方订单 ID 的判断 做与小游戏服务器第一次回调
	if pay.BackUrl != "" {
		if pay.NoticeTimes <= 5 {
			go common.HandleBackUrlWithGameServer(pay)
		}
	}
}

//给客户端的微信验证
func WechatGenSign(key string, m map[string]string) (string, error) {
	var signData []string
	for k, v := range m {
		if v != "" && k != "sign" && k != "key" {
			signData = append(signData, fmt.Sprintf("%s=%s", k, v))
		}
	}

	sort.Strings(signData)
	signStr := strings.Join(signData, "&")
	signStr = signStr + "&key=" + key

	c := md5.New()
	_, err := c.Write([]byte(signStr))
	if err != nil {
		return "", errors.New("WechatGenSign md5.Write: " + err.Error())
	}
	signByte := c.Sum(nil)
	if err != nil {
		return "", errors.New("WechatGenSign md5.Sum: " + err.Error())
	}
	return strings.ToUpper(fmt.Sprintf("%x", signByte)), nil
}
