package wxutil

import (
	"web_pay/model"
	"web_pay/util"
	"web_pay/config"
)

func ToWxServerRequent(code string) (model.WxResultOK, bool){
	var ok bool = true

	json, curlErr := util.Curl(ToGetWxServerReqUrl(code), "")

	if nil != curlErr{
		util.PrintLogger(3, "获取公众号的openid有误", curlErr)
		ok = false
	}

	var wxRes model.WxResultOK

	if jsonErr := util.JsonDecode(json, &wxRes); jsonErr != nil{
		util.PrintLogger(3, "转换公众号的openid有误", jsonErr)
		ok = false
	}

	return wxRes, ok
}


func ToGetWxServerReqUrl(code string) string{
	var WxServerRequest string

	WxServerRequest += "https://api.weixin.qq.com/sns/oauth2/access_token?appid="+config.WXAPPID
	WxServerRequest += "&secret="+config.WXSECRET
	WxServerRequest += "&code="+code
	WxServerRequest += "&grant_type=authorization_code"

	return WxServerRequest
}