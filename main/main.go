package main

import (
	"github.com/labstack/gommon/log"
	"os"
	"web_pay/util"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"web_pay/model"
	"web_pay/controller/v_1_0_0"
	"web_pay/wxutil"
)

func main() {

	env := os.Args[1]

	db := util.Dbinit(env) //连接数据库
	defer db.Close()

	// 连接旧版本的大数据数据库
	// w_tongji 的数据
	//wDb := util.WDbinit(env)
	//defer wDb.Close()

	// redis
	util.RedisInit(env)

	//把数据库里面需要做回调的记录加入服务器缓存
	//go common.AddPayListToCacheFromDatabase()

	// echo framework
	app := echo.New()
	app.Use(middleware.Recover())
	app.Use(middleware.Secure())
	app.Use(middleware.CORS())
	app.Logger.SetLevel(log.OFF)

	appGroup := util.InitRouter(env, app)

	//设置服务器类型
	util.SetServerType(util.IsRemote)
	util.SetServerPort("8924")
	HttpRouter(appGroup)

	serverAddress := util.GetServerAddress()
	util.PrintLogger(1, "", "网页支付服务启动")
	app.Logger.Fatal(app.Start(serverAddress))
}

func HttpRouter(app *echo.Group) {
	util.PrintLogger(1, "收到请求:", app)

	app.Any("/", func(c echo.Context) error {
		var msg model.Msg

		version := c.FormValue("v")
		action := c.FormValue("action")

		switch version{
			case "1.0.0":
				msg = v_1_0_0.Base(c)
			default:
				msg.Msg = "非法的版本号"
		}

		msg.Cmd = action

		if msg.Data == nil{
			msg.Data = ""
		}

		return c.JSON(200, msg)
	})

	//如果收到的是微信内打开的回调
	app.Any("/wxpay", wxutil.JSNotifyCallBack)
}
