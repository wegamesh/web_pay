package v_1_0_0

import (
	"github.com/labstack/echo"
	"web_pay/model"
	"web_pay/util"
	"web_pay/dao"
	"strconv"
	"time"
)

/**
 * 1.0.0的支付校验, 加入三方订单 ID，平台订单 ID
 * @param: gameKey    string  游戏 key
 * @param: deviceType string  设备信息   0: Android 1: iOS
 * @param: platformOrderId string 平台订单 ID
 * @param: orderId string 三方订单 ID
 * 校验情况有四种
 *		1. platformOrderId 和 orderId 都为空，走老的校验方式
 *		2. platformOrderId 存在，以 platformOrderId 为准做校验
 *		3. orderId 存在，以 orderId 为准做校验
 *		4. platformOrderId 和 orderId 都存在，以 platformOrderId 为准做校验
 */
func CheckRecharge(c echo.Context) model.Msg{
	var msg model.Msg

	userId := c.FormValue("userId")
	gameKey := c.FormValue("gameKey")
	deviceType := c.FormValue("deviceType")

	platformOrderId := c.FormValue("platformOrderId") // 平台订单 ID
	thirdOrderId := c.FormValue("orderId")            // 三方订单 ID

	util.PrintLogger(1, "走入v1.0.2 的 CheckRecharge 订单校验接口=====")
	util.PrintLogger(1, "userId====", userId)
	util.PrintLogger(1, "gameKey====", gameKey)
	util.PrintLogger(1, "deviceType====", deviceType)
	util.PrintLogger(1, "platformOrderId====", platformOrderId)
	util.PrintLogger(1, "orderId====", thirdOrderId)

	//校验下gameKey
	if "" == gameKey{
		msg.Msg = "非法的gameKey"
		return msg
	}
	gameId, gameIdError := dao.SelectGameIdByGameKey(gameKey, deviceType)

	if nil != gameIdError{
		msg.Msg = "数据库繁忙，稍后再试"
		return msg
	}

	if gameId == 0{
		msg.Msg = "非法的gameKey"
		return msg
	}

	if "" != platformOrderId && "0" != platformOrderId{
		//走通过平台订单校验
		msg = CheckRechageByPlatformOrderId(platformOrderId)
	}else{
		if "" != thirdOrderId && "0" != thirdOrderId{
		//三方不为空，则通过三方订单校验
			msg = CheckRechageByThreeOrderId(gameKey, deviceType, thirdOrderId)
		}else{
		//三方和平台订单都是空，走早前版本
			msg = OlderCheckRechargeMode(userId, gameKey, deviceType)
		}
	}
	return msg
}


/**
 * 以三方的订单号为准
 * @param: gameKey    	string 游戏 key
 * @param: deviceType 	string 设备信息
 * @param: threeOrderId string 三方订单号
 */
func CheckRechageByThreeOrderId(gameKey, deviceType, threeOrderId string) model.Msg {
	var msg model.Msg

	util.PrintLogger(4, "以三方订单方式校验*** CheckRechageByThreeOrderId", threeOrderId)

	if "" == gameKey {
		msg.Code = 1
		msg.Msg = "参数错误"
		return msg
	}

	// 根据 gameKey 换取 gameId
	gameId, selectGameErr := dao.SelectGameIdByGameKey(gameKey, deviceType)
	if nil != selectGameErr {
		msg.Code = 1
		msg.Msg = "数据库繁忙，请稍后再试"
		util.PrintLogger(3, "SelectGameIdByGameKey 错误", selectGameErr)
		return msg
	}

	util.PrintLogger(1, "CheckRecharge的 gameId", gameId)

	if gameId == 0 {
		msg.Code = 1
		msg.Msg = "非法的 gameKey"
		util.PrintLogger(3, "非法的 gameKey", gameKey)
		return msg
	}

	if "" == deviceType {
		deviceType = "1"
	}

	payInfo, err := dao.GetWxPayByThreeOrderId(strconv.FormatUint(gameId, 10), threeOrderId)

	util.PrintLogger(4, "交易信息", payInfo, "错误", err)
	if err != nil || payInfo.Id == 0 {
		msg.Code = 1
		msg.Msg = "没有这样的交易信息"
		util.PrintLogger(4, "没有这样的交易信息", err)
		return msg
	}

	// fix 判断订单是否被使用过
	if 0 == payInfo.OrderState {
		// 订单已被使用过
		msg.Code = 2
		msg.Msg = "该订单已被使用"
		util.PrintLogger(4, "该订单已被使用", err)
		return msg
	}

	payInfoData := make(map[string]interface{})
	payInfoData["userId"] = payInfo.UserId
	payInfoData["payMoney"] = payInfo.Paymoney
	payInfoData["synced"] = payInfo.Synced
	payInfoData["finishedTime"] = payInfo.Finishedtime
	payInfoData["value"] = payInfo.Value
	payInfoData["exvalue"] = payInfo.Exvalue
	payInfoData["gameKey"] = gameKey
	payInfoData["noticeState"] = payInfo.NoticeState
	payInfoData["orderState"] = payInfo.OrderState // 订单状态 0: 订单是失效（已被使用过） 1: 订单有效

	util.PrintLogger(1, "支付信息数据======", payInfoData)

	// 置换 orderState 的状态
	orderState := 0
	updateErr := dao.UpdateWxOrderStateByThreeOrderId(threeOrderId, orderState)
	if nil != updateErr {
		msg.Code = -1
		msg.Msg = "数据更新失败"
		util.PrintLogger(3, "数据更新失败", updateErr)
		return msg
	}

	if payInfo.NoticeState {
		msg.Code = 1
		msg.Msg = "已经同步了状态"
		msg.Data = payInfoData
		util.PrintLogger(4, "已经同步了状态", payInfoData)
		return msg
	}

	payInfo.NoticeState = true
	payInfo.OrderState  = orderState
	payInfo.UpdatedAt   = strconv.FormatInt(time.Now().Unix(), 10)
	payInfoData["noticeState"] = payInfo.NoticeState
	err1 := dao.SetWxPayLastOrder(payInfo)
	if err1 != nil {
		msg.Code = -1
		msg.Msg = "数据保存错误"
		msg.Data = payInfo
		util.PrintLogger(4, "数据保存错误", err1)
		return msg
	}

	msg.Msg = "success"
	msg.Data = payInfoData
	msg.Code = 0
	util.PrintLogger(1, "success====", payInfoData)

	return msg
}

/**
 * 以寻光支付平台校验为准
 * @param: platformOrderId string
 */
func CheckRechageByPlatformOrderId(platformOrderId string) model.Msg {
	var msg model.Msg

	util.PrintLogger(4, "V1.0.2 已平台订单方式校验*** CheckRechageByPlatformOrderId", platformOrderId)

	payInfo, err := dao.GetWxPayRecordByPlatformOrderId(platformOrderId)

	util.PrintLogger(1, "根据 platformOrderId 获取支付信息***", payInfo)

	if err != nil || payInfo.Id == 0 {
		msg.Code = 1
		msg.Msg = "没有这样的交易信息"
		util.PrintLogger(4, "没有这样的交易信息", err)
		return msg
	}

	// fix 判断订单是否被使用过
	if 0 == payInfo.OrderState {
		// 订单已被使用过
		msg.Code = 2
		msg.Msg = "该订单已被使用"
		util.PrintLogger(4, "没有这样的交易信息", err)
		return msg
	}

	payInfoData := make(map[string]interface{})
	payInfoData["userId"] = payInfo.UserId
	payInfoData["payMoney"] = payInfo.Paymoney
	payInfoData["synced"] = payInfo.Synced
	payInfoData["finishedTime"] = payInfo.Finishedtime
	payInfoData["value"] = payInfo.Value
	payInfoData["exvalue"] = payInfo.Exvalue
	payInfoData["noticeState"] = payInfo.NoticeState
	payInfoData["orderState"] = payInfo.OrderState // 订单状态 0: 订单是失效（已被使用过） 1: 订单有效

	util.PrintLogger(4, "payInfoDatapayInfoData====", payInfoData)

	// 置换 orderState 的状态
	orderState := 0
	updateErr := dao.UpdateWxOrderStateByPlatformOrderId(platformOrderId, orderState)
	if nil != updateErr {
		msg.Code = -1
		msg.Msg = "数据更新失败"
		util.PrintLogger(3, "数据更新失败", updateErr)
		return msg
	}

	if payInfo.NoticeState {
		msg.Code = 1
		msg.Msg = "已经同步了状态"
		msg.Data = payInfoData
		util.PrintLogger(4, "已经同步了状态", payInfoData)
		return msg
	}

	payInfo.NoticeState = true
	payInfo.OrderState  = orderState
	payInfo.UpdatedAt   = strconv.FormatInt(time.Now().Unix(), 10)
	payInfoData["noticeState"] = payInfo.NoticeState
	err1 := dao.SetWxPayLastOrder(payInfo)
	if err1 != nil {
		msg.Code = -1
		msg.Msg = "数据保存错误"
		msg.Data = payInfo
		util.PrintLogger(4, "数据保存错误", err1)
		return msg
	}

	msg.Msg = "success"
	msg.Data = payInfoData
	msg.Code = 0

	util.PrintLogger(1, "checkRecharge===success", msg)

	return msg
}

/**
 * 以旧方式校验支付
 * @param: userId   	string 	游戏用户的 id
 * @param: gameKey  	gameKey 游戏key
 * @param: deviceType 	设备类型 0: Android 1: iOS
 */
func OlderCheckRechargeMode(userId, gameKey, deviceType string) model.Msg {
	var msg model.Msg

	util.PrintLogger(1, "v1.0.2 的旧方式支付校验 OlderCheckRechargeMode***")

	if "" == gameKey || "" == userId {
		msg.Code = 1
		msg.Msg = "参数错误，gameKey 或 userId 不能为空"
		return msg
	}

	// 根据 gameKey 换取 gameId
	gameId, selectGameErr := dao.SelectGameIdByGameKey(gameKey, deviceType)
	if nil != selectGameErr {
		msg.Code = 1
		msg.Msg = "数据库繁忙，请稍后再试"
		util.PrintLogger(1, "selectGameErr 查询出错===", selectGameErr)
		return msg
	}

	util.PrintLogger(1, "CheckRecharge的 gameId", gameId)

	if gameId == 0 {
		msg.Code = 1
		msg.Msg = "非法的 gameKey"
		util.PrintLogger(1, "非法的 gameKey", msg)
		return msg
	}

	if "" == deviceType {
		deviceType = "1"
	}

	payInfo, err := dao.GetWxPayLastOrder(userId, strconv.FormatUint(gameId, 10))
	util.PrintLogger(4, userId, "交易信息", payInfo, "错误", err)
	if err != nil || payInfo.Id == 0 {
		msg.Code = 1
		msg.Msg = "没有这样的交易信息"
		util.PrintLogger(4, userId, "没有这样的交易信息", err)
		return msg
	}

	// fix 判断订单是否被使用过
	if 0 == payInfo.OrderState {
		// 订单已被使用过
		msg.Code = 2
		msg.Msg = "该订单已被使用"
		util.PrintLogger(3, userId, "该订单已被使用", msg)
		return msg
	}

	payInfoData := make(map[string]interface{})
	payInfoData["userId"] = payInfo.UserId
	payInfoData["payMoney"] = payInfo.Paymoney
	payInfoData["synced"] = payInfo.Synced
	payInfoData["finishedTime"] = payInfo.Finishedtime
	payInfoData["value"] = payInfo.Value
	payInfoData["exvalue"] = payInfo.Exvalue
	payInfoData["gameKey"] = gameKey
	payInfoData["noticeState"] = payInfo.NoticeState
	payInfoData["orderState"] = payInfo.OrderState // 订单状态 0: 订单是失效（已被使用过） 1: 订单有效

	util.PrintLogger(1, userId, "支付信息数据", payInfoData)

	// 置换 orderState 的状态
	orderState := 0
	updateErr := dao.UpdateWxOrderStateByUserGameId(userId, strconv.FormatUint(gameId, 10), orderState)
	if nil != updateErr {
		msg.Code = -1
		msg.Msg = "数据更新失败"
		util.PrintLogger(3, userId, "数据更新失败", updateErr)
		return msg
	}

	if payInfo.NoticeState {
		msg.Code = 1
		msg.Msg = "已经同步了状态"
		msg.Data = payInfoData
		util.PrintLogger(4, userId, "已经同步了状态", payInfoData)
		return msg
	}
	payInfo.NoticeState = true
	payInfo.OrderState  = 0
	payInfo.UpdatedAt   = strconv.FormatInt(time.Now().Unix(), 10)
	payInfoData["noticeState"] = payInfo.NoticeState
	err1 := dao.SetWxPayLastOrder(payInfo)
	if err1 != nil {
		msg.Code = -1
		msg.Msg = "数据保存错误"
		msg.Data = payInfo
		util.PrintLogger(4, userId, "数据保存错误", err1)
		return msg
	}

	msg.Msg = "success"
	msg.Data = payInfoData
	msg.Code = 0
	util.PrintLogger(1, userId, "success", payInfoData)

	return msg
}




