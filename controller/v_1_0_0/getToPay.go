package v_1_0_0

import (
	"github.com/labstack/echo"
	"web_pay/model"
	"web_pay/util"
	"web_pay/dao"
	"strconv"
)
/*******
	微信端支付的请求处理
********/
func WebUnifiedOrder(c echo.Context) model.Msg{
	var msg model.Msg

	util.PrintLogger(1, "进入 ====WebUnifiedOrder=====")

	code         	:= c.FormValue("code")					// 微信 code
	gameKey      	:= c.FormValue("gameKey")       		// 游戏 key
	deviceType   	:= c.FormValue("deviceType")		  	// 设备类型   0: Android   1: iOS
	platFormOrderId	:= c.FormValue("platformOrderId")		// xg平台订单
	thirdOrderId := c.FormValue("threeOrderId")				// 三方订单号

	if gameKey == "" || platFormOrderId == "" || thirdOrderId == ""{
		msg.Msg = "参数错误"
		return msg
	}

	//先查找有没有对应的gameId
	gameId, gameIdError := dao.SelectGameIdByGameKey(gameKey, deviceType)

	if nil != gameIdError{
		msg.Msg = "数据库繁忙，稍后再试"
		return msg
	}

	if gameId == 0{
		msg.Msg = "非法的gameKey"
		return msg
	}

	//校验三方订单是否存在
	wxPayId, _ := dao.SelIsExistThreeOrderId(gameId, thirdOrderId)

	if wxPayId.ID != 0 && wxPayId.Synced == 1{
		msg.Msg = "该笔订单已完成，请重新下单"
		return msg
	}

	msg = JSUnifiedOrder(code, platFormOrderId, thirdOrderId, strconv.FormatUint(gameId, 10))

	util.PrintLogger(1, "获得微信的订单")

	return msg
}

/*******
	网页端支付的请求处理
********/
func WebOuterUnifiedOrder(c echo.Context) model.Msg{
	var msg model.Msg

	util.PrintLogger(1, "进入 ====WebUnifiedOrder=====")

	gameKey      	:= c.FormValue("gameKey")       		// 游戏 key
	deviceType   	:= c.FormValue("deviceType")		  	// 设备类型   0: Android   1: iOS
	platFormOrderId	:= c.FormValue("platformOrderId")		// xg平台订单
	thirdOrderId := c.FormValue("threeOrderId")				// 三方订单号

	if gameKey == "" || platFormOrderId == "" || thirdOrderId == ""{
		msg.Msg = "参数错误"
		return msg
	}

	//先查找有没有对应的gameId
	gameId, gameIdError := dao.SelectGameIdByGameKey(gameKey, deviceType)

	if nil != gameIdError{
		msg.Msg = "数据库繁忙，稍后再试"
		return msg
	}

	if gameId == 0{
		msg.Msg = "非法的gameKey"
		return msg
	}

	//校验三方订单是否存在
	wxPayId, _ := dao.SelIsExistThreeOrderId(gameId, thirdOrderId)

	if wxPayId.ID != 0 && wxPayId.Synced == 1{
		msg.Msg = "该笔订单已完成，请重新下单"
		return msg
	}

	msg = WEBUnifiedOrder(platFormOrderId, thirdOrderId, strconv.FormatUint(gameId, 10))

	util.PrintLogger(1, "获得微信的订单")

	return msg
}