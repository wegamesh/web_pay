package v_1_0_0

import (
	"web_pay/model"
	"github.com/labstack/echo"
	"web_pay/util"
	"web_pay/dao"
	"strconv"
	"time"
	"crypto/md5"
	"io"
	"fmt"
)

func GenWebOrderInfo(c echo.Context) model.Msg{
	var msg model.Msg
	//先拿出参数
	gameKey 		:= c.FormValue("gameKey")           //第三方游戏的唯一标示
	userId  		:= c.FormValue("userId")            //用户的唯一标示
	uuid            := c.FormValue("uuid")
	channelKey      := c.FormValue("channelKey")
	adKey           := c.FormValue("adKey")
	threeOrderId 	:= c.FormValue("orderId")            //第三方的orderId
	openId          := c.FormValue("openId")
	payMoney		:= c.FormValue("payMoney")
	value 			:= c.FormValue("value")
	exValue 		:= c.FormValue("exValue")			// 用户支付方做兼容的参数
	deviceType 		:= c.FormValue("deviceType")

	util.PrintLogger(1, "value======", value)
	if "" == gameKey || "" == openId || "" == threeOrderId || "" == userId || "" == payMoney ||
		"" == value || "" == deviceType {
		util.PrintLogger(3, " === 参数错误 === ")
		msg.Msg = "参数错误"
		return msg
	}

	//校验游戏的gameKey,
	gameInfo, selectGameErr := dao.SelectGameInfo(gameKey, deviceType)
	util.PrintLogger(1, "gameInfo=====", gameInfo)


	if nil != selectGameErr || gameInfo.ID == 0{
		util.PrintLogger(3, "获取gameId参数错误=====", gameInfo)
		msg.Msg = "非法的gameKey"
		return msg
	}


	gameId := gameInfo.ID
	gameAppid := gameInfo.GameAppid
	plid := gameInfo.PlId
	payBackUrl := gameInfo.PaybackUrl
	gameName := gameInfo.GameName
	pictUrl := gameInfo.PicUrl

	//获取商品的描述
	valueInt, _ := strconv.Atoi(value)
	produDesc := gameInfo.ExValue
	productArr := util.SplitStrByComma(produDesc)
	proDesc := productArr[valueInt-1]

	//检查当前的三方订单是否已经存在，防止重复下单
	if "" != threeOrderId{
		wxPayOrderId, _ := dao.SelIsExistThreeOrderId(gameId, threeOrderId)
		//订单号存在
		if wxPayOrderId.ID != 0{
			//订单号存在并且已经支付成功了
			if wxPayOrderId.Synced == 1{
				util.PrintLogger(3, "该笔订单已经下过了，请勿重复下单=====", wxPayOrderId)
				msg.Msg = "该笔订单已完成，请返回游戏重新下单"
				return msg
			}else{
				util.PrintLogger(3, "订单异常=====", wxPayOrderId)
				msg.Msg = "订单异常，稍后再试"
				return msg
			}
		}
	}

	intPayMoney, _:= strconv.Atoi(payMoney)

	var data = make(map[string]interface{})

	var wxPayInfo model.WxPayInfo
	wxPayInfo.Synced = 0
	wxPayInfo.PlId = plid
	wxPayInfo.DeviceType = deviceType
	wxPayInfo.AdKey = adKey
	wxPayInfo.Appid = gameAppid
	wxPayInfo.PlatformOrderId = GenXGUniOrderId(userId)
	wxPayInfo.ThreeOrderId = threeOrderId
	wxPayInfo.PayType = 3                                //web支付
	wxPayInfo.OpenId = openId
	wxPayInfo.H5Openid = ""
	wxPayInfo.UserId = userId

	wxPayInfo.Paymoney = strconv.Itoa(intPayMoney * 100)		// 单位是分
	wxPayInfo.MchId = ""
	wxPayInfo.Package = ""
	wxPayInfo.NonceStr = ""
	wxPayInfo.TimeStamp =""
	wxPayInfo.SignType = "MD5"
	wxPayInfo.PaySign = ""
	wxPayInfo.PrepayID = ""
	wxPayInfo.Value = value
	wxPayInfo.Exvalue = exValue
	wxPayInfo.GameId = strconv.FormatUint(gameId, 10)
	wxPayInfo.ChannelKey = channelKey
	wxPayInfo.Uuid = uuid
	wxPayInfo.OrderState = 1
	wxPayInfo.CreatedAt = strconv.FormatInt(time.Now().Unix(), 10)
	wxPayInfo.UpdatedAt = strconv.FormatInt(time.Now().Unix(), 10)
	wxPayInfo.BackUrl = payBackUrl
	wxPayInfo.NoticeTimes = 0

	util.PrintLogger(1, "统一下订单====", wxPayInfo)

	err1 := dao.CreateWxPayOrder(wxPayInfo)

	if nil != err1{
		util.PrintLogger(3, "保存订单出错：====", err1)
		msg.Code = -8
		msg.Msg = "保存订单出错"
		return msg
	}

	data["platformOrderId"] = wxPayInfo.PlatformOrderId
	data["orderId"] = threeOrderId
	data["gameName"] = gameName
	data["pictUrl"] = pictUrl
	data["productDesc"] = proDesc

	msg.Code = 1
	msg.Msg = "数据返回成功"
	msg.Data = data

	return msg
}

//生成平台的唯一订单id

func GenXGUniOrderId(userId string) string{
	unitOrderId := ""

	nanoTimeStamp := time.Now().UnixNano()

	initMd5 := md5.New()
	io.WriteString(initMd5, userId+strconv.FormatInt(nanoTimeStamp, 10))

	unitOrderId = fmt.Sprintf("%x", initMd5.Sum(nil))

	return unitOrderId
}