package v_1_0_0

import (
	"web_pay/model"
	"github.com/labstack/echo"
	"web_pay/util"
	"web_pay/dao"
	)

func XyxCheckOrder(c echo.Context) model.Msg{
	var msg model.Msg

	userId       := c.FormValue("userId")
	gameKey      := c.FormValue("gameKey")
	deviceType   := c.FormValue("deviceType")
	thirdOrderId := c.FormValue("orderId")            // 三方订单 ID

	util.PrintLogger(1, "XyxCheckOrder====", userId)

	if "" != gameKey{
		msg.Msg = "非法的gameKey"
		return msg
	}

	//先查找有没有对应的gameId
	gameId, gameIdError := dao.SelectGameIdByGameKey(gameKey, deviceType)

	if nil != gameIdError{
		msg.Msg = "数据库繁忙，稍后再试"
		return msg
	}

	if gameId == 0{
		msg.Msg = "非法的gameKey"
		return msg
	}

	//intGameId := strconv.FormatUint(gameId, 10)

	payInfo, err := dao.SelIsExistThreeOrderId(gameId, thirdOrderId)

	if err != nil{
		if err.Error() == "record not found"{
			msg.Msg = "未找到对应的订单号"
			return msg
		}else {
			msg.Msg = "数据库繁忙，稍后再试"
			return msg
		}
	}

	if payInfo.Synced == 1{
		msg.Code = 1
		msg.Msg = "支付成功"
	}else{
		msg.Code = 0
		msg.Msg = "支付失败"
	}

	return msg
}
