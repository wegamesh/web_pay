package v_1_0_0

import (
	"web_pay/model"
	"web_pay/cache"
	"web_pay/wxutil"
	"web_pay/util"
	"web_pay/dao"
	"web_pay/config"
	"strconv"
	"os"
	"time"
	"sort"
	"fmt"
	"crypto/md5"
	"strings"
	"encoding/hex"
	"encoding/xml"
	"net/http"
	"bytes"
	"io/ioutil"
)

func JSUnifiedOrder(code, platFormOrderId, threeOrderId, gameId string)model.Msg{
	var msg model.Msg

	//从缓存中获取openID
	openID, openErr := cache.GetOpenid(code)
	if nil != openErr{
		//获取微信公众号的openId
		wxopen, ok := wxutil.ToWxServerRequent(code)
		if !ok || wxopen.Errcode != 0{
			msg.Code = -1
			msg.Msg = "获取openId有误"
			return msg
		}
		openID = wxopen.Openid

		if openID == ""{
			msg.Code = -2
			msg.Msg = "获取的openId为空"
			return msg
		}

		setErr := cache.SetOpenid(code, openID)
		util.PrintLogger(1, "缓存openId", setErr, openID)
	}

	//获取订单信息
	preOrderInfo, preOrderErr := dao.SelectAllPayInfoByGameIdAndPlatFormOrderId(gameId, platFormOrderId)

	if nil != preOrderErr{
		msg.Code = -1
		msg.Msg  = "获取预置订单信息失败"
		return msg
	}
	//获取充钱数
	moneyNum, _ := strconv.Atoi(preOrderInfo.Paymoney)

	var orderReq model.UnifyOrderReq

	orderReq.Appid = config.WXAPPID
	orderReq.Openid = openID
	orderReq.Body = "微信支付"
	orderReq.Mch_id = config.MCHID
	orderReq.Nonce_str = wxutil.GetRandomString(16)

	env := os.Args[1]
	if env == "production" {
		orderReq.Notify_url = "https://wxgame.xunguanggame.com/" + config.ROUTER_PRD + "/wxpay"
	} else {
		orderReq.Notify_url = "https://wxgame.xunguanggame.com/" + config.ROUTER_QA + "/wxpay"
	}

	orderReq.Trade_type = "JSAPI"
	orderReq.Total_fee = moneyNum //单位是分
	orderReq.Out_trade_no = strconv.FormatInt(time.Now().Unix(), 10)
	orderReq.Sign_type = "MD5"

	var m map[string]interface{}
	m = make(map[string]interface{}, 0)
	m["appid"] = orderReq.Appid
	m["openid"] = orderReq.Openid
	m["body"] = orderReq.Body
	m["mch_id"] = orderReq.Mch_id
	m["nonce_str"] = orderReq.Nonce_str
	m["notify_url"] = orderReq.Notify_url
	m["trade_type"] = orderReq.Trade_type
	m["total_fee"] = orderReq.Total_fee
	m["out_trade_no"] = orderReq.Out_trade_no
	m["sign_type"] = orderReq.Sign_type

	orderReq.Sign = WxpayCalcSign(m, config.MCHSECRET) //这个是计算wxpay签名的函数上面已贴出

	//转成xml格式
	bytes_req, err := xml.Marshal(orderReq)
	if err != nil {
		util.PrintLogger(3, "", "以xml形式编码发送错误, 原因:", err)
		msg.Code = -3
		msg.Msg = "以xml形式编码发送错误"
		return msg
	}
	str_req := string(bytes_req)
	//xml里面的UnifyOrderReq无用的信息，用xml替换
	str_req = strings.Replace(str_req, "UnifyOrderReq", "xml", -1)
	bytes_req = []byte(str_req)

	//发送unified order请求.
	req, err := http.NewRequest("POST", "https://api.mch.weixin.qq.com/pay/unifiedorder", bytes.NewReader(bytes_req))
	if err != nil {
		util.PrintLogger(3, "", "New Http Request发生错误，原因:", err)
		msg.Code = -4
		msg.Msg = "New Http Request发生错误"
		return msg
	}

	req.Header.Set("Accept", "application/xml")
	//这里的http header的设置是必须设置的.
	req.Header.Set("Content-Type", "application/xml;charset=utf-8")

	//向微信发送请求
	c := http.Client{}
	resp, _err := c.Do(req)
	if _err != nil {
		util.PrintLogger(3, "", "请求微信支付统一下单接口发送错误, 原因:", _err)
		msg.Code = -5
		msg.Msg = "请求微信支付统一下单接口发送错误"
		return msg
	}

	defer resp.Body.Close()

	//解析微信返回信息
	body, err1 := ioutil.ReadAll(resp.Body)
	if err1 != nil {
		util.PrintLogger(3, "", "读取微信接口返回错误, 原因:", err1)
		msg.Code = -6
		msg.Msg = "读取微信接口返回错误"
		return msg
	}

	wxResult := model.WeChatQueryResult{}

	//把xml转成json格式
	err2 := xml.Unmarshal(body, &wxResult)

	util.PrintLogger(1, "wxResult: 微信数据：", wxResult)

	if err2 != nil {
		util.PrintLogger(3, "", "解析微信返回信息错误, 原因:", err2)
		msg.Code = -7
		msg.Msg = "解析微信返回信息错误"
		return msg
	}

	//产生微信验证，发送给客户端
	var data = make(map[string]interface{})
	data["appId"] = config.WXAPPID
	data["package"] = "prepay_id=" + wxResult.PrepayID
	data["nonceStr"] = wxutil.GetRandomString(16)
	data["timeStamp"] = fmt.Sprintf("%d", time.Now().Unix())
	data["signType"] = "MD5"
	data["paySign"] = WxpayCalcSign(data, config.MCHSECRET)
	//data["prepayId"] = wxResult.PrepayID

	preOrderInfo.H5Openid = orderReq.Openid
	preOrderInfo.MchId = config.MCHID
	preOrderInfo.Appid = config.WXAPPID
	preOrderInfo.Package = "prepay_id=" + wxResult.PrepayID
	preOrderInfo.NonceStr = data["nonceStr"].(string)
	preOrderInfo.TimeStamp = data["timeStamp"].(string)
	preOrderInfo.SignType = "MD5"
	preOrderInfo.PaySign = data["paySign"].(string)
	preOrderInfo.PrepayID = wxResult.PrepayID
	preOrderInfo.UpdatedAt = strconv.FormatInt(time.Now().Unix(), 10)


	util.PrintLogger(3, "统一下订单的数据:======", preOrderInfo)


	updateErr := dao.SaveWxPayInfo(preOrderInfo)
	if updateErr != nil {
		util.PrintLogger(3, preOrderInfo.UserId, "更新旧的订单信息失败", updateErr)
		msg.Msg = "更新旧的订单信息失败"

	} else {
		data["platformOrderId"] = preOrderInfo.PlatformOrderId
		data["orderId"] = threeOrderId

		msg.Code = 1
		msg.Msg = "数据返回成功"
		msg.Data = data
	}

	return msg
}

func WEBUnifiedOrder(platFormOrderId, threeOrderId, gameId string) model.Msg{
	var msg model.Msg

	//获取订单信息
	preOrderInfo, preOrderErr := dao.SelectAllPayInfoByGameIdAndPlatFormOrderId(gameId, platFormOrderId)

	if nil != preOrderErr{
		msg.Code = -1
		msg.Msg  = "获取预置订单信息失败"
		return msg
	}
	//获取充钱数
	moneyNum, _ := strconv.Atoi(preOrderInfo.Paymoney)


	var orderReq model.UnifyOrderReq

	orderReq.Appid = config.WXAPPID
	orderReq.Body = "微信支付"
	orderReq.Mch_id = config.WMCHID
	orderReq.Nonce_str = wxutil.GetRandomString(16)

	env := os.Args[1]
	if env == "production" {
		orderReq.Notify_url = "https://wxgame.xunguanggame.com/" + config.ROUTER_PRD + "/wxpay"
	} else {
		orderReq.Notify_url = "https://wxgame.xunguanggame.com/" + config.ROUTER_QA + "/wxpay"
	}

	orderReq.Trade_type = "MWEB"
	orderReq.Total_fee = moneyNum //单位是分
	orderReq.Out_trade_no = strconv.FormatInt(time.Now().Unix(), 10)
	orderReq.Sign_type = "MD5"

	m := make(map[string]interface{}, 0)
	m["appid"] = orderReq.Appid
	m["body"] = orderReq.Body
	m["mch_id"] = orderReq.Mch_id
	m["nonce_str"] = orderReq.Nonce_str
	m["notify_url"] = orderReq.Notify_url
	m["trade_type"] = orderReq.Trade_type
	m["total_fee"] = orderReq.Total_fee
	m["out_trade_no"] = orderReq.Out_trade_no
	m["spbill_create_ip"] =  "sss"
	m["scene_info"] = "sss"

	orderReq.Sign = WxpayCalcSign(m, config.WMCHSECRET) //这个是计算wxpay签名的函数上面已贴出

	//转成xml格式
	bytes_req, err := xml.Marshal(orderReq)
	if err != nil {
		util.PrintLogger(3, "", "以xml形式编码发送错误, 原因:", err)
		msg.Code = -3
		msg.Msg = "以xml形式编码发送错误"
		return msg
	}

	str_req := string(bytes_req)
	//xml里面的UnifyOrderReq无用的信息，用xml替换
	str_req = strings.Replace(str_req, "UnifyOrderReq", "xml", -1)
	bytes_req = []byte(str_req)

	//发送unified order请求.
	req, err := http.NewRequest("POST", "https://api.mch.weixin.qq.com/pay/unifiedorder", bytes.NewReader(bytes_req))
	if err != nil {
		util.PrintLogger(3, "", "New Http Request发生错误，原因:", err)
		msg.Code = -4
		msg.Msg = "New Http Request发生错误"
		return msg
	}

	req.Header.Set("Accept", "application/xml")
	//这里的http header的设置是必须设置的.
	req.Header.Set("Content-Type", "application/xml;charset=utf-8")

	//向微信发送请求
	c := http.Client{}
	resp, _err := c.Do(req)
	if _err != nil {
		util.PrintLogger(3, "", "请求微信支付统一下单接口发送错误, 原因:", _err)
		msg.Code = -5
		msg.Msg = "请求微信支付统一下单接口发送错误"
		return msg
	}

	defer resp.Body.Close()

	//解析微信返回信息
	body, err1 := ioutil.ReadAll(resp.Body)
	if err1 != nil {
		util.PrintLogger(3, "", "读取微信接口返回错误, 原因:", err1)
		msg.Code = -6
		msg.Msg = "读取微信接口返回错误"
		return msg
	}

	wxResult := model.WebQueryResult{}

	//把xml转成json格式
	err2 := xml.Unmarshal(body, &wxResult)

	util.PrintLogger(1, "wxResult: 微信数据：", wxResult)

	if err2 != nil {
		util.PrintLogger(3, "", "解析微信返回信息错误, 原因:", err2)
		msg.Code = -7
		msg.Msg = "解析微信返回信息错误"
		return msg
	}

	//产生微信验证，发送给客户端
	var data = make(map[string]interface{})
	data["appId"] = config.WXAPPID
	data["package"] = "prepay_id=" + wxResult.PrepayID
	data["nonceStr"] = wxutil.GetRandomString(16)
	data["timeStamp"] = fmt.Sprintf("%d", time.Now().Unix())
	data["signType"] = "MD5"
	data["paySign"] = WxpayCalcSign(data, config.WMCHSECRET)

	preOrderInfo.H5Openid = orderReq.Openid
	preOrderInfo.MchId = config.WMCHID
	preOrderInfo.Appid = config.WXAPPID
	preOrderInfo.Package = "prepay_id=" + wxResult.PrepayID
	preOrderInfo.NonceStr = data["nonceStr"].(string)
	preOrderInfo.TimeStamp = data["timeStamp"].(string)
	preOrderInfo.SignType = "MD5"
	preOrderInfo.PaySign = data["paySign"].(string)
	preOrderInfo.PrepayID = wxResult.PrepayID
	preOrderInfo.UpdatedAt = strconv.FormatInt(time.Now().Unix(), 10)


	util.PrintLogger(3, "统一下订单的数据:======", preOrderInfo)


	updateErr := dao.SaveWxPayInfo(preOrderInfo)
	if updateErr != nil {
		util.PrintLogger(3, preOrderInfo.UserId, "更新旧的订单信息失败", updateErr)
		msg.Msg = "更新旧的订单信息失败"

	} else {
		data["platformOrderId"] = preOrderInfo.PlatformOrderId
		data["orderId"] = threeOrderId

		msg.Code = 1
		msg.Msg = "数据返回成功"
		msg.Data = data
	}

	return msg
}

//wxpay计算签名的函数
func WxpayCalcSign(mReq map[string]interface{}, key string) (sign string) {
	//fmt.Println("微信支付签名计算, API KEY:", key)
	//STEP 1, 对key进行升序排序.
	sorted_keys := make([]string, 0)
	for k, _ := range mReq {
		sorted_keys = append(sorted_keys, k)
	}

	sort.Strings(sorted_keys)

	//STEP2, 对key=value的键值对用&连接起来，略过空值
	var signStrings string
	for _, k := range sorted_keys {
		//fmt.Printf("k=%v, v=%v\n", k, mReq[k])
		value := fmt.Sprintf("%v", mReq[k])
		if value != "" {
			signStrings = signStrings + k + "=" + value + "&"
		}
	}

	//STEP3, 在键值对的最后加上key=API_KEY
	if key != "" {
		signStrings = signStrings + "key=" + key
	}
	//fmt.Println("signStrings", signStrings)
	//STEP4, 进行MD5签名并且将所有字符转为大写.
	md5Ctx := md5.New()
	md5Ctx.Write([]byte(signStrings))
	cipherStr := md5Ctx.Sum(nil)
	upperSign := strings.ToUpper(hex.EncodeToString(cipherStr))
	return upperSign
}