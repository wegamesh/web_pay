package v_1_0_0

import (
	"github.com/labstack/echo"
	"web_pay/model"
)

func Base(c echo.Context) model.Msg{
	var msg model.Msg

	action := c.FormValue("action")

	switch action{
	//生成预订单
	case "preGenOrder":
		msg = GenWebOrderInfo(c)

	//h5支付请求
	case "webUnifiedOrder":
		msg = WebUnifiedOrder(c)

	//网页端请求支付
	case "webOuterUnifiedOrder":
		msg = WebOuterUnifiedOrder(c)

	//小程序检查是否支付成功
	case "xyxCheckOrder":
		msg = XyxCheckOrder(c)

	//三方检查是否支付成功
	case "checkRecharge":
		msg = CheckRecharge(c)

	default:
		msg.Msg = "请求参数错误"
	}

	return msg
}