package common

import (
	"encoding/json"
	"github.com/labstack/gommon/log"
	"happycube_go_api/config"
	"io/ioutil"
	"net/http"
	"net/url"
)

type TokenRes struct {
	Code int
	Msg  string
	Data string
	Cmd  string
}

func GetAccessToken() TokenRes {
	tokenResult := TokenRes{}
	resp, err := http.PostForm("https://pkwegame.xunguanggame.com/token-go/", url.Values{"action": {"getToken"}, "appId": {config.APPID}})
	if err != nil {
		return tokenResult
	}
	defer resp.Body.Close()
	body, err1 := ioutil.ReadAll(resp.Body)
	if err1 != nil {
		return tokenResult
	}
	err2 := json.Unmarshal(body, &tokenResult)
	if err2 != nil {
		return tokenResult
	}

	log.Info("GetGameTokenFromTokenServer----", tokenResult)
	return tokenResult
}

// 从寻光公共服务中获取 accessToken
func GetCommonAccessToken(appId string) TokenRes {
	tokenResult := TokenRes{}
	resp, err := http.PostForm("https://pkwegame.xunguanggame.com/token-go/", url.Values{"action": {"getToken"}, "appId": {appId}})
	if err != nil {
		return tokenResult
	}
	defer resp.Body.Close()
	body, err1 := ioutil.ReadAll(resp.Body)
	if err1 != nil {
		return tokenResult
	}
	err2 := json.Unmarshal(body, &tokenResult)
	if err2 != nil {
		return tokenResult
	}

	log.Info("GetGameTokenFromTokenServer----", tokenResult)
	return tokenResult
}



