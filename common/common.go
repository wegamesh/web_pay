package common

import (
	"encoding/json"
	"web_pay/dao"
	"web_pay/model"
	"web_pay/util"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"
)

type CachePayListInfo struct {
	WxPayInfo  model.WxPayInfo //需要回调的数据库记录
	UpdateTime int64           //上次回调的时间戳
}

var BackPayListInfoCache = NewPayInfoCache()

func NewPayInfoCache() ListCache {
	var p = ListCache{}
	p.payList = make(map[int64]CachePayListInfo, 0)
	return p
}

type ListCache struct {
	payList map[int64]CachePayListInfo
	mu      sync.RWMutex
}

//把需要支付回调的记录加入服务器缓存
func AddPayInfoToServerCache(tmp model.WxPayInfo) {
	BackPayListInfoCache.mu.Lock()
	payInfo := CachePayListInfo{}
	payInfo.WxPayInfo = tmp
	payInfo.UpdateTime = time.Now().Unix()
	BackPayListInfoCache.payList[tmp.Id] = payInfo
	BackPayListInfoCache.mu.Unlock()
}

//删除服务器缓存中需要回调的记录信息
func DeleteServerCachePayList(tmp model.WxPayInfo) {
	BackPayListInfoCache.mu.Lock()
	for _, elem := range BackPayListInfoCache.payList {
		if elem.WxPayInfo.Id == tmp.Id {
			delete(BackPayListInfoCache.payList, elem.WxPayInfo.Id)
		}
	}
	BackPayListInfoCache.mu.Unlock()
}

/**
 * 与小游戏服务器通信（回调给小游戏服务器，通知充值成功）
 */
func HandleBackUrlWithGameServer(tmp model.WxPayInfo) {
	if tmp.NoticeState == true {
		return
	}
	status := 0
	//  BackUrl + 订单号  backUrl=https://wxgame.xunguanggame. com/pixelalbum-go/payback?platformOrderId=xxxxx&orderId=498812&userId=aaaaa
	payBackUrl := tmp.BackUrl + "platformOrderId=" + tmp.PlatformOrderId + "&orderId=" + tmp.ThreeOrderId + "&userId=" +
		tmp.UserId + "&paymoney=" + tmp.Paymoney + "&value=" + tmp.Value + "&deviceType=" + tmp.DeviceType
	util.PrintLogger(1, "回调的url 地址=====", payBackUrl)
	resp, err := http.Post(payBackUrl , "application/x-www-form-urlencoded", strings.NewReader("name=cjb"))

	// 通信次数加1
	tmp.NoticeTimes += 1
	util.PrintLogger(3, "回调的通信次数", tmp.NoticeTimes)

	util.PrintLogger(3, "保存回调的所有信息", tmp)
	err4 := dao.SaveWxPayOrder(tmp)
	if err4 != nil {
		util.PrintLogger(3, "请求小程序参数", tmp, "保存用户信息到数据库更新修改与小游戏服务器通信次数", err4)
	}

	if tmp.NoticeTimes >= 5 {
		// 通信次数超过5次，不在通知
		DeleteServerCachePayList(tmp)
	} else {
		// 更新缓存中的内存
		AddPayInfoToServerCache(tmp)
	}

	util.PrintLogger(3, "缓存更新后的回调数据=====", tmp)

	if err != nil {
		util.PrintLogger(3, "请求小程序参数", tmp, "请求小程序服务器出错", err)
		status = 1
		return
	}

	util.PrintLogger(3, "请求小程序服务器状态码", resp.StatusCode)
	if resp.StatusCode != 200 {
		util.PrintLogger(3, "请求小程序参数", tmp, "请求小程序服务器状态出错", resp.StatusCode)
		status = 2
		return
	}

	defer resp.Body.Close()

	body, err1 := ioutil.ReadAll(resp.Body)
	if err1 != nil {
		util.PrintLogger(3, "请求小程序参数", tmp, "读取返回body出错", err1)
		status = 3
		return
	}

	var result int
	err2 := json.Unmarshal(body, &result)
	if err2 != nil {
		util.PrintLogger(3, "请求小程序参数", tmp, "解析body信息出错", err2)
		status = 4
		return
	}

	if result != 1 {
		util.PrintLogger(3, "请求小程序参数", tmp, "返回结果不对", result)
		status = 5
		return
	}


	//通信成功
	if status == 0 {
		tmp.OrderState  = 0
		tmp.NoticeState = true
		tmp.UpdatedAt   = strconv.FormatInt(time.Now().Unix(), 10)
		err3 := dao.SaveWxPayOrder(tmp)
		if err3 != nil {
			util.PrintLogger(3, "请求小程序参数", tmp, "保存用户信息到数据库更新修改已经通知小游戏服务器", err3)
		}
		//回调小游戏成功，就删除服务器中的缓存
		DeleteServerCachePayList(tmp)
	} else {

		err4 := dao.SaveWxPayOrder(tmp)
		if err4 != nil {
			util.PrintLogger(3, "请求小程序参数", tmp, "保存用户信息到数据库更新修改与小游戏服务器通信次数", err4)
		}
		if tmp.NoticeTimes >= 5 {
			util.PrintLogger(3, "回调删除，> 5",  "删除内容====", tmp)
			//通信次数超过5次，不在通知
			DeleteServerCachePayList(tmp)
		} else {
			//通信次数没有超过5次，放入缓存中，继续通知
			AddPayInfoToServerCache(tmp)
		}
	}
}

// 检测需要回调的数据
func AddPayListToCacheFromDatabase() {
	list, err := dao.GetWxPayOrderBySyncedAndNoticeState()
	if err != nil {
		util.PrintLogger(3,"需要回调的记录出错", err)
		return
	}
	if len(list) <= 0 {
		return
	}
	for _, elem := range list {
		//此时的订单在微信已经支付返回成功了&&游戏没有同步成功&&发送次数是小于5的时候才会放入缓存里面
		if elem.Synced == 1 && elem.NoticeState == false && elem.NoticeTimes < 5 {
			// fix: 不校验订单号，当 backUrl 不为空时，加入回调队列
			if elem.BackUrl != "" {
				//满足以上条件的才能放入缓存中
				AddPayInfoToServerCache(elem)
			}
		}
	}
}
