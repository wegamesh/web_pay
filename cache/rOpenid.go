package cache

import (
	"web_pay/constant"
	"web_pay/util"
)

func SetOpenid(code, openid string) error {
	_, err := util.SetExRedis(constant.REDIS_CODE_OPENID + code, openid, 7200)
	return err
}

func GetOpenid(code string) (string, error) {
	str, err := util.GetRedisString(constant.REDIS_CODE_OPENID + code)

	return str, err
}
