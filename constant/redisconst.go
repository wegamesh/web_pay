package constant

const (
	REDIS_NAME          = "web_pay"
	REDIS_TIME          = 360
	REDIS_CODE_OPENID = REDIS_NAME + "codeopenid"
)
