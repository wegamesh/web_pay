package dao

import (
	"web_pay/model"
	"web_pay/util"
	"strconv"
	"time"
)

// 更新微信订单信息
func SaveWxPayInfo (preOrderInfo model.WxPayInfo) error {
	err := util.Db.Save(&preOrderInfo).Error
	return err
}

func SelectGameInfo(gameKey, deviceType string) (model.ScanGameInfo, error){
	var gameInfo model.ScanGameInfo

	selectErr := util.Db.Table("game_infos").Select("id, pl_id, game_appid, game_appsercet, payback_url, game_name, pic_url, ex_value").
		Where("game_key= ? and device_type= ?", gameKey, deviceType).Scan(&gameInfo).Error

	return gameInfo, selectErr
}

func SelIsExistThreeOrderId(gameId uint64, threeOrderId string)(model.ScanWxOrder, error){
	var wxOrder model.ScanWxOrder

	selectErr := util.Db.Table("wx_pay_infos").Select("id, synced").
		Where("three_order_id = ? and game_id = ?", threeOrderId, gameId).Scan(&wxOrder).Error

	return wxOrder, selectErr
}

func CreateWxPayOrder(info model.WxPayInfo) error{
	saveErr := util.Db.Save(&info).Error
	return saveErr
}

func SelectGameIdByGameKey(gameKey, deviceType string) (uint64, error){
	var gameId model.ScanGameId

	selectErr := util.Db.Table("wx_pay_infos").Select("id").
		Where("game_key = ? and device_type = ?", gameKey, deviceType).Scan(&gameId).Error

	return gameId.ID, selectErr
}

// 根据三方订单号获取订单信息
func GetWxPayByThreeOrderId(gameId, threeOrderId string) (model.WxPayInfo, error) {
	info := model.WxPayInfo{}
	err := util.Db.Table("wx_pay_infos").Where("three_order_id=? and synced=1 and game_id= ?", threeOrderId, gameId).First(&info).Error
	return info, err
}

// 根据 threeOrderId 置换微信订单状态
func UpdateWxOrderStateByThreeOrderId(threeOrderId string, orderState int) error {
	updateOrderState := make(map[string]interface{})
	updateOrderState["order_state"] = orderState
	updateOrderState["updated_at"]  = strconv.FormatInt(time.Now().Unix(), 10)
	updateErr := util.Db.Table("wx_pay_infos").Where("three_order_id=? and synced=1", threeOrderId).UpdateColumns(updateOrderState).Error
	return updateErr
}

func SetWxPayLastOrder(tmp model.WxPayInfo) error {
	err := util.Db.Save(&tmp).Error
	return err
}

// 获取平台指定的单号对应的信息
func GetWxPayRecordByPlatformOrderId(platformOrderId string) (model.WxPayInfo, error) {
	info := model.WxPayInfo{}
	err := util.Db.Table("wx_pay_infos").Where("platform_order_id=? and synced=1", platformOrderId).First(&info).Error
	return info, err
}

// 根据 platformOrderId 置换微信订单状态
func UpdateWxOrderStateByPlatformOrderId(platformOrderId string, orderState int) error {
	updateOrderState := make(map[string]interface{})
	updateOrderState["order_state"] = orderState
	updateOrderState["updated_at"] = strconv.FormatInt(time.Now().Unix(), 10)
	updateErr := util.Db.Table("wx_pay_infos").Where("platform_order_id=? and synced=1", platformOrderId).UpdateColumns(updateOrderState).Error
	return updateErr
}

// 获取微信支付的最新的一条订单
func GetWxPayLastOrder(uid, gameId string) (model.WxPayInfo, error) {
	info := model.WxPayInfo{}

	err := util.Db.Table("wx_pay_infos").Where("user_id=? and synced=1 and game_id = ?", uid, gameId).Last(&info).Error
	return info, err
}

// 根据 userId, gameId 置换微信订单状态
func UpdateWxOrderStateByUserGameId(userId, gameId string, orderState int) error {
	updateOrderState := make(map[string]interface{})
	updateOrderState["order_state"] = orderState
	updateOrderState["updated_at"] = strconv.FormatInt(time.Now().Unix(), 10)
	util.Db.Debug().Table("wx_pay_infos").Where("user_id=? and synced=1 and game_id = ?", userId, gameId).UpdateColumns(updateOrderState)
	updateErr := util.Db.Table("wx_pay_infos").Where("user_id=? and synced=1 and game_id = ?", userId, gameId).UpdateColumns(updateOrderState).Error
	return updateErr
}

//  根据 gameId 和 platFormOrderId 获取所有的支付信息
func SelectAllPayInfoByGameIdAndPlatFormOrderId(gameId, platFormOrderId string) (model.WxPayInfo, error) {
	info := model.WxPayInfo{}

	util.Db.Debug().Table("wx_pay_infos").Where("game_id= ? and platform_order_id= ?", gameId,
		platFormOrderId).First(&info)
	err := util.Db.Table("wx_pay_infos").Where("game_id= ? and platform_order_id= ?", gameId,
		platFormOrderId).First(&info).Error

	return info, err
}

func GetWxPayOrderByOpenid(opneid, money string) ([]model.WxPayInfo, error) {
	var orders []model.WxPayInfo
	util.Db.Debug().Where("h5_openid= ? and paymoney= ?", opneid, money).Find(&orders)
	err := util.Db.Where("h5_openid= ? and paymoney= ?", opneid, money).Find(&orders).Error
	return orders, err
}

func SaveWxPayOrder(tmp model.WxPayInfo) error {
	err := util.Db.Save(&tmp).Error
	return err
}

//全表扫描一边，获取已经到账，没有通知小游戏服务器，同时通信次数少于5次
func GetWxPayOrderBySyncedAndNoticeState() ([]model.WxPayInfo, error){
	var orders []model.WxPayInfo
	err := util.Db.Table("wx_pay_infos").Where("synced= 1 and notice_state= 0 and notice_times < 5 and pay_type= 0").Find(&orders).Error
	return orders, err
}